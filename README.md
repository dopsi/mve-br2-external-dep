# MVE: add a dependency in external.mk

[**Reply from Y. Morin**](https://lists.buildroot.org/pipermail/buildroot/2022-March/638193.html)

This repo is a minimal Buildroot external tree to
conditionnally add a dependency from an external.mk file.

To run the test, run the following command from your buildroot
directory:

```
make BR2_EXTERNAL=<path to this directory> test_dopsi_defconfig
```
