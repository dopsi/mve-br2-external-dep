################################################################################
#
# stm32wrapper4dbg
#
################################################################################

STM32WRAPPER4DBG_VERSION = st-android-11.0.0-2021-08-31
STM32WRAPPER4DBG_SITE = $(call github,STMicroelectronics,stm32wrapper4dbg,$(STM32WRAPPER4DBG_VERSION))
STM32WRAPPER4DBG_LICENSE = GPL-2.0+ or BSD-3-Clause
STM32WRAPPER4DBG_LICENSE_FILES = COPYING LICENSE-BSD-3-CLAUSE LICENSE-GPL2

define HOST_STM32WRAPPER4DBG_BUILD_CMDS
    $(HOST_MAKE_ENV) $(MAKE) $(HOST_CONFIGURE_OPTS) -C $(@D) stm32wrapper4dbg
endef

define HOST_STM32WRAPPER4DBG_INSTALL_CMDS
    $(INSTALL) -D -m 755 $(@D)/stm32wrapper4dbg $(HOST_DIR)/usr/bin/stm32wrapper4dbg
    $(INSTALL) -D -m 644 $(@D)/stm32wrapper4dbg.1 $(HOST_DIR)/usr/share/man/man1/stm32wrapper4dbg.1
endef

$(eval $(host-generic-package))

