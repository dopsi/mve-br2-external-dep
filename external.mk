include $(sort $(wildcard $(BR2_EXTERNAL_dopsi_PATH)/package/*/*.mk))

ifeq ($(dopsi_DEBUG_FSBL),y)
define ARM_TRUSTED_FIRMWARE_SPL_WRAPPER
       $(foreach f,$(call qstrip,$(BR2_TARGET_ARM_TRUSTED_FIRMWARE_IMAGES)), \
               for i in $(ARM_TRUSTED_FIRMWARE_IMG_DIR)/$(f) ; do \
               cp $$i $$i.orig ;\
               $(TARGET_MAKE_ENV) \
               stm32wrapper4dbg -s $$i.orig -d $$i ;\
               done \
        )
endef
ARM_TRUSTED_FIRMWARE_POST_BUILD_HOOKS += ARM_TRUSTED_FIRMWARE_SPL_WRAPPER
ARM_TRUSTED_FIRMWARE_DEPENDENCIES += host-stm32wrapper4dbg
endif

